from bottle import run, request, response, get, error
import Cabarrubias_Sieve, json


@get('/sieve')
def index():
	to_return = None
	limit = request.query.limit
	if limit.isdigit() or (limit.find('-') == 0 and limit[1:].isdigit()):
		try:
			to_return = Cabarrubias_Sieve.sift(int(limit))
		except:
			to_return = error_400()
	else:
		to_return = error_400()
	return '<h1>' + json.dumps({'result': to_return}) + '</h1>'


@error(400)
def error_400():
	response.status = 400
	return 'Something went wrong.'



run(host='10.2.1.72', port=8080)